import { LightningElement, api, wire } from 'lwc';
import getAccounts from '@salesforce/apex/GetAccountData.getAccounts';
export default class ConfigureLWC extends LightningElement {
   @api accRating ;
    @api acclimit ;
    @wire(getAccounts, { accRating: 'hot' , acclimit:10 })
    accounts;
}