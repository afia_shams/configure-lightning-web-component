public with sharing class GetAccountData {
    @AuraEnabled(cacheable=true)
    // get accounts records based on rating passed
    public  static List<Account> getAccounts(String accRating, Integer acclimit) {
            return [Select Id , Name , Type,Rating from Account
              WHERE Rating =:accRating Limit :acclimit] ;
            }
             
        }
